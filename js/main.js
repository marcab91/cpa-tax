var isChrome = !!window.chrome && !!window.chrome.webstore;

$(document).ready(function()
{
    closeIframe();
});

function destaca (id)
{
	if (id == 'home_boton_iva') { $("#maincircle").attr( 'src', 'images/btn_iva.png'); }
	if (id == 'home_boton_isr') { $("#maincircle").attr( 'src', 'images/btn_isr.png'); }
	if (id == 'home_boton_ieps') { $("#maincircle").attr( 'src', 'images/btn_ieps.png'); }
	if (id == 'home_boton_isan') { $("#maincircle").attr( 'src', 'images/btn_isan.png'); }	
	if (id == 'home_boton_ietu') { $("#maincircle").attr( 'src', 'images/btn_ietu.png'); }
}


function showMenu()
{
	$('.menu-item').slideToggle(350);
}

function restaura()
{
	$("#maincircle").attr('src','images/circulo.png');
}


function openIframe(url)
{
            if(isChrome)
            {
                $("#blur").foggy({
                    blurRadius: 15,         // In pixels.
                    opacity: 0.5,           // Falls back to a filter for IE.
                    cssFilterSupport: true  // Use "-webkit-filter" where available.
                });
            }
            else
            {
                $("body").append('<div id="peor_es_nada"></div>');
                $("#peor_es_nada").css('background-color',"#fff");
                $("#peor_es_nada").css('opacity',"0.90");
                $("#peor_es_nada").css('top',"0");
                $("#peor_es_nada").css('left',"0");
                $("#peor_es_nada").css('width',"100%");
                $("#peor_es_nada").css('height',"100%");
                $("#peor_es_nada").css('display',"block");
                $("#peor_es_nada").css('position',"absolute");
                $("#peor_es_nada").css('z-index',"21474");

            }

            $("#launcher").attr('src',url);
            $("#launcher").show(250);
            $("#close_config").show(50);
}

function closeIframe()
{
    if(isChrome){$("#blur").foggy(false);}
    else
    {
        $("#peor_es_nada").remove();
    }

    $("#launcher").hide();
}